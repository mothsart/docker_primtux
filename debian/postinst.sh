#!/bin/sh

if [ -n "$(cat /etc/os-release | grep Debian)" ]; then
    echo 'debian'
fi

if [ -n "$(cat /etc/os-release | grep Ubuntu)" ]; then
    echo 'ubuntu'
fi 
