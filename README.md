# Tests automatisés de Primtux sur Docker

## Intro

Installer docker

## Lancement sur une base Debian

```bash
docker build -t primtux/debian debian
```

```bash
docker run -i -t primtux/debian /bin/bash
```

## Lancement sur une base Ubuntu

```bash
docker build -t primtux/ubuntu ubuntu
```

```bash
docker run -i -t primtux/ubuntu /bin/bash
```
